#!/bin/bash

PROJECT_DIR="sources/FPGA-design/script_support/components/CAPE"
PROJECT_NAME="PICO-INST"
[ ! -z "${1}" ] && infile="${1}" || infile="$PROJECT_DIR/$PROJECT_NAME/test_program.c"

ARCH=rv32im
ROM=2048
RAM=256
STACK=64

CFLAGS=" -march=$ARCH -mabi=ilp32 -Os -g3 -flto -Wall"
LDFLAGS=" -march=$ARCH -mabi=ilp32 -Os -g3 -flto"
LDFLAGS+=" -Wl,--gc-sections,--defsym=__flash=0x00000000,--defsym=__flash_size=$ROM"
LDFLAGS+=" -Wl,--defsym=__ram=0x02000000,--defsym=__ram_size=$RAM,--defsym=__stack_size=$STACK"

riscv64-unknown-elf-gcc $CFLAGS -c "$infile" -o .temp.file.o && \
riscv64-unknown-elf-gcc $LDFLAGS -o .temp.file.elf .temp.file.o && \
riscv64-unknown-elf-objdump -d .temp.file.elf -M no-aliases,numeric > machine_code.lss && \
riscv64-unknown-elf-objcopy -O binary .temp.file.elf .temp.file.bin && \
truncate -s $ROM .temp.file.bin && \
riscv64-unknown-elf-objcopy -O ihex .temp.file.elf $PROJECT_DIR/$PROJECT_NAME/program_dump.hex && \
riscv64-unknown-elf-size -B --common .temp.file.elf && \

echo "program_dump.hex and machine_code.lss have been successfully generated from $infile"  || \
echo "And therefore, $infile could not be converted into binary format. :("

rm -f .temp.file.*
