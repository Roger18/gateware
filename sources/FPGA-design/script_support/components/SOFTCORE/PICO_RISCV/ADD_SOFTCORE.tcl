puts "======== Add SOFTCORE option: PICORV ========" 

#-------------------------------------------------------------------------------
# Import HDL source files
#-------------------------------------------------------------------------------
import_files -hdl_source {script_support/components/SOFTCORE/PICO_RISCV/HDL/BVF_WRAPPER.v}
import_files -hdl_source {script_support/components/SOFTCORE/PICO_RISCV/HDL/IO_CONTROLLER.v}
import_files -hdl_source {script_support/components/SOFTCORE/PICO_RISCV/HDL/PICORV32.v}

build_design_hierarchy

create_hdl_core -file $project_dir/hdl/BVF_WRAPPER.v -module {SOFTCORE} -library {work} -package {}

#-------------------------------------------------------------------------------
# Build the Softcore module
#-------------------------------------------------------------------------------
set sd_name ${top_level_name}

#-------------------------------------------------------------------------------
# Instantiate.
#-------------------------------------------------------------------------------

sd_instantiate_hdl_core -sd_name ${sd_name} -hdl_core_name {SOFTCORE} -instance_name {SOFTCORE} 

#Add Program Memory for PRU
sd_instantiate_component -sd_name ${sd_name} -component_name {PF_TPSRAM_C0} -instance_name {PROGRAM_MEM}

#-------------------------------------------------------------------------------
# Connections.
#-------------------------------------------------------------------------------

# Clocks and resets
sd_connect_pins -sd_name ${sd_name} -pin_names {"CLOCKS_AND_RESETS:FIC_3_PCLK" "SOFTCORE:CLK"}
sd_connect_pins -sd_name ${sd_name} -pin_names {"CLOCKS_AND_RESETS:FIC_3_FABRIC_RESET_N" "SOFTCORE:RESETN" }
# CAPE
sd_connect_pins -sd_name ${sd_name} -pin_names {"CAPE:GPIO_IN" "SOFTCORE:GPIO_IN"}
sd_connect_pins -sd_name ${sd_name} -pin_names {"SOFTCORE:GPIO_OE" "CAPE:GPIO_OE"}
sd_connect_pins -sd_name ${sd_name} -pin_names {"SOFTCORE:GPIO_OUT" "CAPE:GPIO_OUT"}

#BVF RISCV Subsystem
#Program Memory
sd_connect_pins -sd_name ${sd_name} -pin_names {"CLOCKS_AND_RESETS:FIC_3_PCLK" "PROGRAM_MEM:CLK"}
sd_connect_pins -sd_name ${sd_name} -pin_names {"SOFTCORE:INSTR_ADDR" "PROGRAM_MEM:R_ADDR"}
sd_connect_pins -sd_name ${sd_name} -pin_names {"PROGRAM_MEM:R_DATA" "SOFTCORE:INSTR_DATA"}
sd_connect_pins -sd_name ${sd_name} -pin_names {"SOFTCORE:INSTR_VALID" "PROGRAM_MEM:R_EN"}
sd_connect_pins_to_constant -sd_name ${sd_name} -pin_names {PROGRAM_MEM:W_ADDR} -value {GND}
sd_connect_pins_to_constant -sd_name ${sd_name} -pin_names {PROGRAM_MEM:W_DATA} -value {GND}
sd_connect_pins_to_constant -sd_name ${sd_name} -pin_names {PROGRAM_MEM:W_EN} -value {GND} 


sd_mark_pins_unused -sd_name ${sd_name} -pin_names {BVF_RISCV_SUBSYSTEM:GPIO_2_M2F}
sd_mark_pins_unused -sd_name ${sd_name} -pin_names {BVF_RISCV_SUBSYSTEM:GPIO_2_OE_M2F}
sd_connect_pins_to_constant -sd_name ${sd_name} -pin_names {BVF_RISCV_SUBSYSTEM:GPIO_2_F2M} -value {GND} 