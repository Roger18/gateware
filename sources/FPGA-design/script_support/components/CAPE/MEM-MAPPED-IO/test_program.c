#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

// main function
int main() {
        // Address value of variables are updated for RISC-V Implementation
        #define LED_4         (* (volatile bool * ) 0x03000004)
        LED_4 = true;
    return 0;
}

