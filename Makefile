MICROCHIP_DIR = /home/$(USER)/Microchip
LIBERO_PROJECT_NAME = BVF_GATEWARE_025T

python-tools: gitpython
	pip3 -y install $^

build-tools: lsb git 
	sudo apt install $^

build-project:
	. $(MICROCHIP_DIR)/setup-microchip-tools.sh
	python3 build-bitstream.py ./$(filter-out $@,$(MAKECMDGOALS))
	libero work/libero/$(LIBERO_PROJECT_NAME).prjx

open-project:
	libero work/libero/$(LIBERO_PROJECT_NAME).prjx